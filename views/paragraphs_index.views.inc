<?php

/**
 * Implements hook_views_data().
 */
function paragraphs_index_views_data() {
  $data['paragraphs_index']['table']['group']  = t('Paragraphs item');
  $data['paragraphs_index']['table']['join'] = array(
    'paragraphs_item' => array(
      'left_field' => 'item_id',
      'field' => 'item_id',
    ),
    'node' => array(
      'left_field' => 'nid',
      'field' => 'entity_id',
    ),
  );
  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function paragraphs_index_views_data_alter(&$data) {
  $data['node']['paragraphs_item_node_item_id'] = array(
    'title' => t('Paragraph items on node'),
    'help' => t('Relate nodes to paragraph items. This relationship will cause duplicated records if there are multiple paragraphs.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship_node_paragraphs_data',
      'label' => t('paragraph'),
      'base' => 'paragraphs_item',
    ),
  );
}
