<?php

/**
 * @file
 * Definition of views_handler_relationship_node_paragraphs_data.
 */

/**
 * Relationship handler to return the paragraph items of entities.
 *
 * @ingroup views_relationship_handlers
 */
class views_handler_relationship_node_paragraphs_data extends views_handler_relationship  {
  function query() {
    $this->ensure_my_table();

    $def = $this->definition;
    $def['table'] = 'paragraphs_item';

    $paragraphs_index = $this->query->add_table('paragraphs_index', $this->relationship);
    $def['left_table'] = $paragraphs_index;
    $def['left_field'] = 'item_id';
    $def['field'] = 'item_id';
    $def['type'] = empty($this->options['required']) ? 'LEFT' : 'INNER';
    $def['extra'][] = array(
      'table' => 'paragraphs_index',
      'field' => 'entity_type',
      'value' => 'node',
    );

    $join = new views_join();

    $join->definition = $def;
    $join->construct();
    $join->adjusted = TRUE;

    $alias = $def['table'] . '_' . $this->table;
    $this->alias = $this->query->add_relationship($alias, $join, 'paragraphs_item', $this->relationship);
  }
}
