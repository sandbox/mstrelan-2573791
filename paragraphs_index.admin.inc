<?php

/**
 * Form builder callback.
 */
function paragraphs_index_configuration($form, &$form_state) {
  $form['paragraphs_index_recursive'] = array(
    '#title' => t('Recursive indexing'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('paragraphs_index_recursive', FALSE),
    '#description' => t('If checked nested paragraphs will also be included in the index.'),
  );
  $form = system_settings_form($form);
  $form['actions']['rebuild'] = array(
    '#type' => 'submit',
    '#value' => t('Rebuild index'),
    '#submit' => array('paragraphs_index_configuration_rebuild_submit'),
  );
  return $form;
}

function paragraphs_index_configuration_rebuild_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/paragraphs/index/rebuild';
}

function paragraphs_index_rebuild_confirm() {
  return confirm_form(array(), t('Are you sure you want to rebuild the paragraphs index?'),
    'admin/structure/paragraphs/index',
    t('This action rebuilds the paragraphs index on all entities and may be a lengthy process. This action cannot be undone.'),
    t('Rebuild index'),
    t('Cancel'));
}

function paragraphs_index_rebuild_confirm_submit($form, &$form_state) {
  paragraphs_index_rebuild();
  $form_state['redirect'] = 'admin/structure/paragraphs/index';
}

function paragraphs_index_rebuild() {
  db_delete('paragraphs_index')->execute();
  $batch = array(
    'title' => t('Rebuilding paragraphs index'),
    'operations' => array(
      array('_paragraphs_index_rebuild_batch_operation', array()),
    ),
    'finished' => '_paragraphs_index_rebuild_batch_finished',
    'file' => drupal_get_path('module', 'paragraphs_index') . '/paragraphs_index.admin.inc',
  );
  batch_set($batch);
}

function _paragraphs_index_rebuild_batch_operation(&$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['all_entities'] = _paragraphs_index_rebuild_get_entities();
    $context['sandbox']['max'] = count($context['sandbox']['all_entities']);
  }
  $limit = 20;
  $entities = array_slice($context['sandbox']['all_entities'], $context['sandbox']['progress'], $limit);
  foreach ($entities as $entity_info) {
    if ($results = entity_load($entity_info['entity_type'], array($entity_info['entity_id']))) {
      $entity = reset($results);
      paragraphs_index_build_index($entity, $entity_info['entity_type']);
    }
    $context['sandbox']['progress']++;
  }
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

function _paragraphs_index_rebuild_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('The paragraphs index has been rebuilt.'));
  }
  else {
    drupal_set_message(t('The paragraphs index has not been properly rebuilt.'), 'error');
  }
  cache_clear_all();
}

function _paragraphs_index_rebuild_get_entities() {
  $entities = array();
  $fields = array_keys(field_read_fields(array(
    'module' => 'paragraphs',
    'type' => 'paragraphs',
  )));
  foreach ($fields as $field_name) {
    $info = field_info_field($field_name);
    foreach (array_keys($info['bundles']) as $entity_type) {
      $query = new EntityFieldQuery();
      $result = $query->entityCondition('entity_type', $entity_type)
        ->fieldCondition($field_name, NULL, '!=')
        ->execute();

      foreach ($result as $entity_type => $rows) {
        foreach (array_keys($rows) as $entity_id) {
          $entities[$entity_type][$entity_id] = $entity_id;
        }
      }
    }
  }
  return _paragraphs_index_rebuild_flatten_entities($entities);
}

function _paragraphs_index_rebuild_flatten_entities($data) {
  $flattened = array();
  foreach ($data as $entity_type => $entities) {
    foreach (array_keys($entities) as $entity_id) {
      $flattened[] = array(
        'entity_type' => $entity_type,
        'entity_id' => $entity_id,
      );
    }
  }
  return $flattened;
}
